# Private Dockerhub - DAIMON Project (TU Clausthal)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

-   [Badges](#badges)
-   [About the Project](#about-the-project)
-   [How to Use](#how-to-use)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Badges

[![pipeline status](https://gitlab.com/tuc_graphql/dockerhub/badges/master/pipeline.svg)](https://gitlab.com/tuc_graphql/dockerhub/commits/master)

## About the Project

Create custom node alpine image with git to support submodule in CI pipelines.

## How to Use

Image need to be included in GitLab CI file `.gitlab-ci.yml`:

```console
image: "registry.gitlab.com/tuc_graphql/dockerhub/node-alpine-git:latest"
```